<%-- 
    Document   : addphone
    Created on : Dec 10, 2018, 9:11:38 AM
    Author     : Nam Nguyen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add phone</title>
    </head>
    <body>
        <table>
            <form method="POST" action="Servlet">
                <tr>
                    <td>Name: </td>
                    <td>
                        <input type="text" name="phoneName"/>
                    </td>
                </tr>
                <tr>
                    <td>Brand: </td>
                    <td>
                        <select name="brand">
                            <option value="apple">Apple</option>
                            <option value="samsung">Samsung</option>
                            <option value="nokia">Nokia</option>
                            <option value="others">Others</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Price: </td>
                    <td>
                        <input type="text" name="price" />
                    </td>
                </tr>
                <tr>
                    <td>Description: </td>
                    <td>
                        <input type="text" name="description" />
                    </td>
                </tr>
                <tr>
                    <td><button type="submit" value="Submit">Submit</button></td>
                    <td><button type="reset" value="Reset">Reset</button></td>
                </tr>
            </form>
        </table>
    </body>
</html>
