/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BO;

import DAO.DAO;
import bean.PhoneInfo;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Nam Nguyen
 */
public class PhoneBO {
    PreparedStatement ps;
    DAO dao = new DAO();
    
    /**
     * add new phone
     */
    public int add (PhoneInfo phone){
       String sql = "INSERT INTO phone (id, name, brand, price, description) VALUES ("+phone.getID()+",'" + phone.getName() + "', '" + phone.getBrand() + "', '" + phone.getPrice() + "', '" + phone.getDescription() + "')";
        DAO dao = new DAO();
        System.out.println(sql);
        return dao.executeUpdateQuery(sql);
    }
    
    /**
     * get list phone
     */
    public List<PhoneInfo> getListPhone (){
        String sql = "select * from PHONE";
        List<PhoneInfo> list = new LinkedList();
        
        try {
            ResultSet rs = dao.executeQuery(sql);
            while (rs.next()) {
                PhoneInfo phone = new PhoneInfo();
                phone.setID(rs.getInt("ID"));
                phone.setName(rs.getString("NAME"));
                phone.setBrand(rs.getString("BRAND"));
                phone.setPrice(rs.getString("PRICE"));
                phone.setDescription(rs.getString("DESCRIPTION"));
                list.add(phone);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            dao.closeConnection();
            return null;
        }
        dao.closeConnection();
        return list;
    }
}
